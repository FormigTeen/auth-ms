OAuth2 MicroService 

Para utilizar o MicroService, você precisa solicitar ao Owner a criação do seu { CLIENT_ID, CLIENT_SECRET } 

## Instruções para o Owner

1. Suba o Banco
`php artisan migrate`

2. Gere as Chaves Pública e Privada, essas chaves serão utilizadas para gerar as Chaves dos Clientes da Aplicação
`php artisan passport:install`

### Gerando as Chaves dos Clientes 

1. Se essa aplicação tivesse uma interação **Humana <-> Servidor**, deveria executar: `php artisan passport:client`, o Comando iria exigir o Nome e URL Redirect para enviar o usuário de volta para aplicação do Client.
2. Essa aplicação é um MicroService e sua interação é exclusiva **Servidor <-> Servid**, então execute o comando `php artisan passport:client --client`, para gerar o { CLIENT_ID, CLIENT_SECRET }

## Instruções para o Client

* `POST` | /oauth/token 
    * **Descrição**: Retorna o `Access_Token`, que deverá ser utilizado nos Headers das Rotas Protegidas
    * **Middleware*:
    * **Corpo**: 
        * `grant_type` - O tipo de autenticação que será utilizada, o único habilitado no MicroService: 
            * `client_credentials` - Utilizado quando o Cliente deseja se Autenticar
        * `client_id` - ID do Cliente que está realizando a Requisição, obtido através do Cadastro pelo Owner
        * `client__secret` - Chave Secreta do Cliente que está realizando a Requisição, obtida através do Cadastro pelo Owner
                 
    * **Resposta**:
        * `token_type` - O Tipo de Autorização OAuth2 que deverá ser utilizada no Header Authorization
        * `expires_in` - Quando o `Access Token` será expirado.
        * `access_token` - O Token de Acesso que deverá ser utilizado na Header das Rotas Protegidas
        
    ### Detalhes de Autorização
    
    Existem 2 Middlewares principais:
    
    * `client` - Os recursos da Rota só podem ser utilizados por Cliente
    * `auth` - Os recursos da Rota só podem ser utilizados por Usuários
    
    Todas as Rotas Protegidas devem ter no Header o Campo `Authorization`:
    
    * `Authorization: Bearer {access_token}`

    A aplicação irá utilizar o `access_token` para validar e identificar quem está realizando a requisição.
    
    ## Recursos
    
    * `GET` | /users 
        * **Descrição**: Solicita a Lista de Usuários cadastrados
        * **Middleware**: `client`
        * **Corpo**:
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            * `users` - Array de Usuáiros
                * `id` - ID do Usuário
                * `name` - Nome do Usuário
                * `email` - Email do Usuário
                * `data` - Dados do Usuário
            
    * `POST` | /register 
        * **Descrição**: Solicita um Cadastro de Usuário
        * **Middleware**: `client`
        * **Corpo**:
            * `email` - Email do Usuário
            * `password` - Senha do Usuário
            * `password_confimation` - Senha de Confirmação do Usuário
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            * `access_token` - O Token de Acesso que deverá ser utilizado na Header das Rotas Protegidas
            
    * `POST` | /login 
        * **Descrição**: Solicita o Login de Usuário
        * **Middleware**: `client`
        * **Corpo**:
            * `email` - Email do Usuário
            * `password` - Senha do Usuário
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            * `access_token` - O Token de Acesso que deverá ser utilizado na Header das Rotas Protegidas

    * `POST` | /logout 
        * **Descrição**: Solicita a Revogação da Chave do Usuário
        * **Middleware**: `auth`
        * **Corpo**:
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            
    * `POST` | /recovery 
        * **Descrição**: Solicita o Envio de Email para o Usuário com a nova Senha
        * **Middleware**: `client`
        * **Corpo**:
            * `email` - Email do Usuário que terá a senha Recuperada
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            
    * `GET` | /auth 
        * **Descrição**: Solicita a Verificação de Login do Usuário
        * **Middleware**: `auth`
        * **Corpo**:
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            * `id` - ID do Usuário
            * `data` - Dados do Usuário
            * `email` - Email do Usuário Logado
            * `name` - Nome do Usuário Logado
            * `client_name` - Nome do Cliente que Solicitou a Verificação
            
    * `POST` | /auth
        * **Descrição**: Atualiza os Dados do Usuário
        * **Middleware**: `auth`
        * **Corpo**:
            * `data` - Dados do Usuário
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            
    * `PUT` | /auth/password
        * **Descrição**: Atualiza a Senha
        * **Middleware**: `auth`
        * **Corpo**:
            * `password` - Senha do Usuário
            * `password_confimation` - Senha de Confirmação do Usuário
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            * `id` - ID do Usuário
            * `data` - Dados do Usuário
            * `email` - Email do Usuário
            * `name` - Nome do Usuário
            * `client_name` - Nome do Cliente que Solicitou a Verificação

    ### Autenticação com Serviços ###
    
    O MicroService dá suporte a utilização de Serviços Externos de Autenticação OAuth2:
    drivers disponíveis:
    `github` 
    
    * `POST` | /socialite/{driver}/register 
        * **Descrição**: Solicita o Cadastro do Serviço na Conta do Clietn
        * **Observação**:O ID e Token do Serviço são obtidos no cadastro de um App OAuth no próprio sistema do Serviço. A URL de Callback da Aplicação deve ser: `URL_MICROSERVICE/socialite/{driver}/{client_id}/callback}`. O MiccroService irá receber os dados necessários para validar o Token.
        * **Middleware**: `client`
        * **Corpo**:
            * `service_id` - ID do Serviço
            * `service_token` - Token de Serviço
            * `redirect_url` - URL de Retorno para receber o Token do Usuário depois do Processo de Validação
        * **Resposta**: 
            * `message` - Mensagem de Resposta

    * `GET` | /socialite/{driver}/redirect 
        * **Descrição**: Solicita a Autenticação no Serviço Externo
        * **Observação**: Essa rota primeiramente envia uma resposta 3XX ( Redireconamento ) para o Serviço utilizado para a autenticação. Se autenticado, o MicroService irá enviar a resposta padrão.
        * **Middleware**: `client`
        * **Corpo**:
        * **Resposta**: 
            * `message` - Mensagem de Resposta
            * `access_token` - O Token de Acesso que deverá ser utilizado na Header das Rotas Protegidas
            
            #### Observação
            
            1. Toda conta **CRIADA** através de serviços terceiros, recebem uma senha automática. Para fazer o Login via Email é necessário que o usuário recupere a senha.
            2. Uma conta pode ter vários serviços associados a ela, se e somente se, todos os serviços tiverem Emails semelhantes. O tratamento é automático.
            
# Futuras Adições #
    
    * Salvar Dados adicionais nos Usuários
    * Retorno de Timestamp do Expired Token
    * Permitir Eventos de Tokens que serão Expirados
