<?php

namespace App;

use App\Exceptions\LoginException;
use App\Exceptions\RegisterException;
use App\Notifications\RecoveryPasswordNotification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\Client;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens, SoftDeletes, Notifiable;

    protected $fillable = [
        'email',
    ];

    public static function rules($isLogin = false)
    {
        return [
            'email' => ['required', 'email', 'string', 'max:255'],
            'password' => ['required', 'string', 'max:255', 'min:6', $isLogin ? null : 'confirmed'],
            'data' => ['nullable', 'string'],
        ];
    }

    public function scopeByClient(Builder $query, $client_id)
    {
        return $query->whereHas('passwords', function (Builder $query) use ($client_id) {
            $query->where('client_id', $client_id);
        })->with(['passwords' => function ($query) use ($client_id) {
            $query->where('client_id', $client_id);
        }]);
    }

    public static function isRegistered(string $email, int $client_id)
    {
        $user = self::getRegistered($email, $client_id);
        if ( empty($user) || empty($user->passwords) )
            return false;
        else
            return $user;
    }

    public static function getRegistered(string $email, int $client_id)
    {
        return self::where('email', $email)->with(['passwords' => function ($query) use ($client_id) {
            $query->where('client_id', $client_id);
        }])->first();
    }

    public static function login(string $email, string $password, int $client_id)
    {
        $user = self::isRegistered($email, $client_id);
        if ( $user && $user->checkPassword($password, $client_id) ) {
            return $user;
        };
        throw new LoginException('User isn\'t registered or Credentials is bad');
    }

    public static function forceRegister(string $email, int $client_id) : self
    {
        $user = self::isRegistered($email , $client_id);
        return ( $user ? $user : self::register($email, Hash::make(Str::random()), $client_id) );
    }

    public function insertService(string $driver, $client_id, object $auth)
    {
        $driver = strtolower($driver);
        $this->getPassword($client_id)->update([
            "tokens->{$driver}" => [
                "code" => $auth->token,
                "expires_in" => $auth->expiresIn,
            ]
        ]);
        return $this;
    }

    public function checkPassword(string $password, int $client_id)
    {
        return Hash::check($password, $this->getPassword($client_id)->password);
    }

    public static function recovery($email, $client_id)
    {
        if ( $user = self::isRegistered($email, $client_id) ) {
            $token = Str::random(16);
            $user->insertPassword($token, $client_id);
            $user->notify(new RecoveryPasswordNotification(Client::findOrFail($client_id), $token));
        } else {
            throw new LoginException('User isn\'t registered or Credentials is bad');
        }
    }

    public function getPassword($client_id)
    {
        return $this->passwords->where('client_id', $client_id)->first();
    }

    public static function register(string $email, string $password, string $client_id)
    {
        if ( !self::isRegistered($email , $client_id) ) {
            return self::create(['email' => $email])->insertPassword($password, $client_id);
        }
        throw new RegisterException('User is Registered!');

    }

    public static function registerService(string $email, string $password, string $client_id)
    {
        if ( !self::isRegistered($email , $client_id) )
        {
            return self::create(['email' => $email])->insertPassword($password, $client_id);
        }

        return null;
    }

    public function insertPassword($password, $client_id) : self
    {
        Password::updateOrCreate([
            'user_id' => $this->id,
            'client_id' => $client_id
        ], [
            'password' => Hash::make($password),
        ]);

        return $this;
    }

    public function insertData($data, $client_id) : self
    {
        Password::where([
            'user_id' => $this->id,
            'client_id' => $client_id
        ])->firstOrFail()->update([
            'data' => $data,
        ]);

        return $this;
    }


    public function passwords()
    {
        return $this->hasMany(Password::class);
    }
}
