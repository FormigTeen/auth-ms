<?php


namespace App\Notifications;


use App\Password;
use App\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Laravel\Passport\Client;

class RecoveryPasswordNotification extends Notification
{
    use Queueable;

    public $password;
    public $client;

    public function __construct(Client $client, string $password)
    {
        $this->password = $password;
        $this->client = $client;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your password was changed!')
            ->greeting('Hello!')
            ->line("Your password was changed on {$this->client->name} application")
            ->line("Your new password is: {$this->password}")
            ->line('Thank you!');
    }

}
