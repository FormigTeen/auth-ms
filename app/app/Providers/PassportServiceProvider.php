<?php

namespace App\Providers;

use Carbon\Carbon;
use Dusterio\LumenPassport\LumenPassport;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class PassportServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        Passport::tokensExpireIn(Carbon::now()->addDays(env('PASSPORT_EXPIRES_TOKEN_DAYS', 15)));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(env('PASSPORT_REFRESH_TOKEN_DAYS', 30)));
        LumenPassport::tokensExpireIn(Carbon::now()->addYears(env('LUMEN_PASSPORT_REFRESH_TOKEN_YEARS', 50)), 2);
    }
}
