<?php

namespace App;

use App\Exceptions\SocialiteException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\GithubProvider;

class Service extends Model
{

    protected $fillable = [
        'client_id', 'service_token', 'type', 'service_id'
    ];

    public static function rules()
    {
        return [
            'service_token' => ['required', 'string'],
            'service_id' => ['required', 'string'],
            'type' => ['required', 'integer', 'min:0', 'max:0'],
        ];
    }

    public const TYPE = [
        '0' => 'github',
    ];

    public function getProviderAttribute()
    {
        switch ($this->type) {
            case 0:
                return GithubProvider::class;
            default:
                throw new SocialiteException('This Driver isn\'t usable or found', 400);
        }
    }

    public function getTypeStrAttribute()
    {
        return self::TYPE[$this->type];
    }

    public function getProviderSocialite()
    {
        return Socialite::buildProvider($this->provider, [
            'client_id' => $this->service_id,
            'client_secret' => $this->service_token,
            'redirect' => env('APP_URL') . "/socialite/{$this->type_str}/{$this->client_id}/callback",
        ]);

    }

    public static function findProvider(string $provider)
    {
        $result = array_search($provider, self::TYPE, false);
        if ( $result == false )
            throw new SocialiteException("Provider not Found!", 400);

        return $result;
    }


    public static function search(int $client_id, string $provider)
    {
        return self::where(['client_id' => $client_id])
            ->where(['type' => self::findProvider($provider)])
            ->firstOrFail();
    }
}
