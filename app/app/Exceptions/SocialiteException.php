<?php

namespace App\Exceptions;

use Throwable;

class SocialiteException extends \Exception
{

    protected $status;

    public function __construct($message = "", $status = 400, $code = 0, Throwable $previous = null)
    {
        $this->status = $status;
        parent::__construct($message, $code, $previous);
    }

    public function report()
    {

    }

    public function render($request)
    {
            return response(
                [
                    'message' => $this->getMessage(),
                ], $this->status
            );
    }

}
