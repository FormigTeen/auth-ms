<?php

namespace App\Exceptions;

class RegisterException extends \Exception
{

    public function report()
    {

    }

    public function render($request)
    {
            return response(
                [
                    'message' => $this->getMessage(),
                ], 400
            );
    }

}
