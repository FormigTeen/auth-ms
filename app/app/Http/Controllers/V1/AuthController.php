<?php

namespace App\Http\Controllers\V1;


use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RecoveryRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\UpdateDataRequest;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\User;
use Illuminate\Http\Request;
use Laravel\Passport\Client;

class AuthController extends Controller
{

    public function login(LoginRequest $request)
    {
        $user = User::login($request->email, $request->password, $request->client->id);
        return response(
            [
                'access_token' => $user->createToken($request->client->id)->accessToken,
                'user_id' => $user->id,
                'message' => 'User was Logged!'
            ], 200
        );
    }

    public function register(RegisterRequest $request)
    {
        $user = User::register($request->email, $request->password, $request->client->id)->insertData($request->data, $request->client->id);
        return response(
            [
                'access_token' => $user->createToken($request->client->id)->accessToken,
                'user_id' => $user->id,
                'message' => 'User was Registered!',
            ], 201
        );
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response([
            'message' => 'You have been succesfully logged out!'
        ], 200);
    }

    public function auth(Request $request)
    {
        $client = Client::findOrFail($request->user()->token()->name);
        return response([
            'message' => 'You are logged!',
            'email' => $request->user()->email,
            'id' => $request->user()->id,
            'data' => $request->user()->getPassword($client->id)->data,
            'client_name' => $client->name,
        ], 200);
    }

    public function recovery(RecoveryRequest $request)
    {
        User::recovery($request->email, $request->client->id);
        return response([
            'message' => 'User\'s password sent to email!',
        ], 200);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $request->user()->insertPassword($request->password, $request->user()->token()->name);
        return response([
            'message' => 'User\'s password was changed!',
        ], 200);
    }

    public function update(UpdateDataRequest $request)
    {
        $user = $request->user()->insertData($request->data, $request->user()->token()->name);
        $client = Client::findOrFail($request->user()->token()->name);
        return response([
            'message' => 'User\'s data was changed!',
            'email' => $user->email,
            'id' => $user->id,
            'data' => $user->getPassword($client->id)->data,
            'client_name' => $client->name,
        ], 200);
    }

}
