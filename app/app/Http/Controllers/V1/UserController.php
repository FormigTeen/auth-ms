<?php

namespace App\Http\Controllers\V1;



use App\User;
use Illuminate\Http\Request;
use Laravel\Passport\Client;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::byClient($request->client->id)->get();
        return response([
            'users' => $users->map(function (User $user) {
            return ['email' => $user->email, 'data' => $user->passwords->first()->data, 'id' => $user->id];
        })->toArray(),
            'message' => 'Request success!',
        ],
        200);
    }

    public function show(Request $request)
    {
        $user = User::byClient($request->client->id);
        if ( $request->email ) {
            $user = $user->where('email', $request->email);
        }
        if ( $request->id ) {
            $user = $user->where('id', $request->id);
        }
        $user = $user->firstOrFail();

        return response([
            'message' => 'User found',
            'email' => $user->email,
            'id' => $user->id,
            'data' => $user->getPassword($request->client->id)->data,
        ], 200);
    }

}
