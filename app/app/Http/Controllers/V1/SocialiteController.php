<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\Socialite\RegisterRequest;
use App\Service;
use App\User;
use Illuminate\Http\Request;

class SocialiteController extends Controller
{
    public function register(RegisterRequest $request, string $driver)
    {
        Service::updateOrCreate([
            'client_id' => $request->client->id,
            'type' => array_search($driver, Service::TYPE),
        ], [
            'service_id' => $request->service_id,
            'service_token' => $request->service_token,
        ]);

        return response(['message' => "Credentials {$driver} was set"], 200);
    }

    public function redirect(string $driver, Request $request)
    {
        return Service::search($request->client->id, $driver)->getProviderSocialite()
            ->stateless()
            ->redirect();
    }

    public function callback($driver, $client_id, Request $request)
    {
        $service = Service::search($client_id, $driver);
        $userSocialite = $service->getProviderSocialite()->stateless()->user();
        $user = User::forceRegister($userSocialite->getEmail(), $client_id)->insertService($driver, $client_id, $userSocialite);

        return response(
            [
                'access_token' => $user->createToken($client_id)->accessToken,
                'user_id' => $user->id,
                'message' => 'User was Logged!'
            ], 200
        );
    }
}
