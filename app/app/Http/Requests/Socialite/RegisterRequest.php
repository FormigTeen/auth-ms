<?php

namespace App\Http\Requests\Socialite;

use App\Service;
use Pearl\RequestValidate\RequestAbstract;

class RegisterRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return Service::rules();
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
