<?php

namespace App\Http\Requests\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use Pearl\RequestValidate\RequestAbstract;

class LoginRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return User::rules(true);
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
