<?php

namespace App\Http\Requests\Auth;

use Pearl\RequestValidate\RequestAbstract;

class UpdatePasswordRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'password' => ['required', 'string', 'max:255', 'min:6', 'confirmed'],
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
