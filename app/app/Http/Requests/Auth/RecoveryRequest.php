<?php

namespace App\Http\Requests\Auth;

use Pearl\RequestValidate\RequestAbstract;

class RecoveryRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', 'max:255', 'exists:users']
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
