<?php

namespace App\Http\Middleware;

use Closure;

class PassportMiddleware
{
    public function handle($request, Closure $next)
    {
        if ( $request->getPathInfo() !== '/oauth/token') {
            return response(['message' => 'Laravel Passport Features Disabled. Look the Docs about this MicroService.'], 401);
        }
        if ( $request->get('grant_type') !== 'client_credentials') {
            return response(['message' => 'Only \'client_credentials\' is active. Look the Docks about this MicroService'], 401);
        }
        $request->request->set('route', $request->getPathInfo() === "/test");

        return $next($request);
    }
}
