<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class Password extends Model
{

    protected $fillable = [
        'user_id', 'password', 'client_id', 'data',
    ];

    protected $hidden = [
        'password',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDataAttribute($value)
    {
        return json_decode($value);
    }

    public function setDataAttribute($value)
    {
        if ( json_decode($value) && json_last_error() === JSON_ERROR_NONE)
            $this->attributes['data'] = $value;
        else
            $this->attributes['data'] = json_encode($value);
    }

}
