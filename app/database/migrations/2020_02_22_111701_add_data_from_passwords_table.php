<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataFromPasswordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('passwords', function (Blueprint $table) {
            $table->json('data')->nullable();
        });
    }

    public function down()
    {
        Schema::table('passwords', function (Blueprint $table) {
            $table->dropColumn('data')->nullable();
        });
    }
}
