<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGithubFromPasswordsTable extends Migration
{
    public function up()
    {
        Schema::table('passwords', function (Blueprint $table) {
            $table->json('tokens')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('passwords', function (Blueprint $table) {
            $table->dropColumn('tokens');
            //
        });
    }
}
