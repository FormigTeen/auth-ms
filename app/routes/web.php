<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return response(['message' => $router->app->version()], 200);
});

$router->group(['prefix' => 'v1', 'namespace' => 'V1'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('/logout', ['uses' => 'AuthController@logout']);
        $router->get('/auth', ['uses' => 'AuthController@auth']);
        $router->put('/auth/password', ['uses' => 'AuthController@updatePassword']);
        $router->put('/auth', ['uses' => 'AuthController@update']);
    });

    $router->group(['middleware' => 'client'], function () use ($router) {
        $router->post('/register', ['uses' => 'AuthController@register']);
        $router->post('/login', ['uses' => 'AuthController@login']);
        $router->post('/recovery', ['uses' => 'AuthController@recovery']);
        $router->get('/users', ['uses' => 'UserController@index']);
        $router->get('/user', ['uses' => 'UserController@show']);
        $router->group(['prefix' => 'socialite/{driver}'], function () use ($router) {
            $router->post('register', ['uses' => 'SocialiteController@register']); // TODO: Rota que irá permitir o Client cadastrar suas Credenciais
            $router->get('redirect', ['uses' => 'SocialiteController@redirect']); // TODO: Rota que irá permitir o Client receber o Link de Redirecionamento
        });
    });

    $router->group(['prefix' => 'socialite/{driver}'], function () use ($router) {
        $router->get('{client_id}/callback', ['uses' => 'SocialiteController@callback']); // TODO: Rota que irá recepcionar o Driver e Preparar a Resposta para o Client
    });
});
